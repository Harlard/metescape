using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : EObject
{
    private bool open;
    private bool isOpening;
    private bool isClosing;
    private bool closed;
    [SerializeField]public bool isLock;
    [SerializeField]public int keyNumber;
    private float check;
    private GameObject player;
    private GameObject help;
    private Help hint;
    private Inventory  inventory;
    [SerializeField]public float speedRotation;
          // Start is called before the first frame update
    void Start()
    {
    open = false;
    closed = true;
    isOpening = false;
    isClosing = false;
    help = GameObject.Find("Help");
    hint = help.GetComponent<Help>();
    if((keyNumber != 0)&&(isLock)){
      player = GameObject.Find("Player");
      inventory = player.GetComponent<Inventory>();
      }


    }

    // Update is called once per frame
    void Update()
    {
      if(isOpening){
        transform.Rotate( 0, speedRotation*Time.deltaTime, 0);
        check += speedRotation*Time.deltaTime;
        if(check >= 90){
          isOpening = false;
          open = true;
          closed = false;
        }
      }
      if(isClosing){
        transform.Rotate( 0, -speedRotation*Time.deltaTime, 0);
        check -= speedRotation*Time.deltaTime;
        if(check <= 0){
          isClosing = false;
          closed = true;
          open = false;
        }
      }


    }
    void checkDoor(){
      if(!isLock){
        if(closed){
          isOpening = true;
        }
        if(open){
          isClosing = true;
        }
      }else{
        bool check = inventory.checkKey(keyNumber);
        if(check){
          isLock = false;
          isOpening = true;
          hint.SetText("Door unlock");
        }else{
          hint.SetText("Door lock");
        }

      }

    }

    public override void  DoThing(){
      checkDoor();
    }
}
