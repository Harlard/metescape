using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEObject {
  void DoThing();
}

public abstract class EObject : MonoBehaviour, IEObject{

  public abstract void DoThing();

  public void Use(){
    DoThing();
  }
}
