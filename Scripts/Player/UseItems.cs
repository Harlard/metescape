using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseItems : MonoBehaviour
{
    private Inventory inventory;
    private StatPlayer stat;
    private FlashlightVM flashlight;
    [SerializeField] int healthAmount;
    [SerializeField] int rechargeAmount;

    void Awake(){
      inventory = GetComponent<Inventory>();
      stat = GetComponent<StatPlayer>();
      flashlight = GetComponent<FlashlightVM>();
    }

    public void UseHealthPack(){
      bool check = inventory.checkInventoryForUse("healthPack");
      if(check){
        inventory.addHealthPack(-1);
        stat.TakeDamage(-healthAmount);
      }
    }

    public void UseAntidote(){
      bool check = inventory.checkInventoryForUse("antidote");
      if(check){
        inventory.addAntidote(-1);
        stat.SetPoison(false);
      }
    }

    public void UseBattery(){
      bool check = inventory.checkInventoryForUse("battery");
      if(check){
        inventory.addBattery(-1);
        flashlight.rechargeBattery(rechargeAmount);
      }
    }
}
