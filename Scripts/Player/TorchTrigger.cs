using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchTrigger : MonoBehaviour
{
    private GameObject player;
    private ControllerM controller;
    bool playerMoving;

    void Awake(){
      player = GameObject.Find("Player");
      controller = player.GetComponent<ControllerM>();
      playerMoving = controller.GetIsRunning();

    }
    void Update()
    {
        playerMoving = controller.GetIsRunning();
        if (playerMoving)
        {
            GetComponent<Animator>().Play("TorchMoving");
        }
        if (!playerMoving)
        {
            GetComponent<Animator>().Play("TorchStill");
        }
    }
}
