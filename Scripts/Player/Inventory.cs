using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
  [SerializeField] private int coins;
  [SerializeField] private int currentBatteries;
  [SerializeField] private int maxBatteries;
  [SerializeField] private int currentHealthPack;
  [SerializeField] private int maxHealthPack;
  [SerializeField] private int currentAntidote;
  [SerializeField] private int maxAntidote;

  [SerializeField] private bool trophy1;
  [SerializeField] private bool pin1;
  [SerializeField] private bool key1;


public void addBattery(int batteriesAmount){
  currentBatteries += batteriesAmount;
}
public void addCoins(int coinsAmount){
  coins += coinsAmount;
}
public void addHealthPack(int healthPackAmount){
  currentHealthPack += healthPackAmount;
}
public void addAntidote(int antidoteAmount){
  currentAntidote += antidoteAmount;
}

public void addKey(int keyN){
    switch(keyN){
      case 1:
        key1  = true;
        break;
      default:
        break;
      }
  }


public void addPin(int pinN){
    switch(pinN){
      case 1:
        pin1  = true;
        break;
      default:
        break;
      }
  }

public void addTrophy(int trophyN){
    switch(trophyN){
      case 1:
        trophy1  = true;
        break;
      default:
        break;
      }
  }

public bool checkInventory(string name){
  if((name == "battery")&&(currentBatteries < maxBatteries)){
    return true;
  }else if((name == "healthPack")&&(currentHealthPack < maxHealthPack)){
    return true;
  }else if((name == "antidote")&&(currentAntidote < maxAntidote)){
    return true;
  }else if(name == "trophy"){
    return true;
  }else if(name == "pin"){
    return true;
  }else{return false;}
}

public bool checkInventoryForUse(string name){
  if((name == "battery")&&(currentBatteries > 0)){
    return true;
  }else if((name == "healthPack")&&(currentHealthPack > 0)){
    return true;
  }else if((name == "antidote")&&(currentAntidote > 0)){
    return true;
  }else{return false;}
}

public int CurrentBatteriesAmount(){
  return currentBatteries;
}

public int CurrentAntidoteAmount(){
  return currentAntidote;
}

public int CurrentHealthPackAmount(){
  return currentHealthPack;
}

public int CurrentCoinsAmount(){
  return coins;
}

public bool checkKey(int keyNumber){
  if((keyNumber == 1)&&(key1 == true)){
    return true;
  }else{
    return false;
  }
}


}
