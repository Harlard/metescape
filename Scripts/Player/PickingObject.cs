using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// This is screen should be set in camera in order to get object in from of it
public class PickingObject : MonoBehaviour
{
    [SerializeField] private float Range;
    private Inventory inventory;
    private GameObject player;
    private string name;
    private bool collectable;
    private bool useable;
    private GameObject objects;
    private PickUp pick;
    private EObject use;

    void Awake(){
      player = GameObject.Find("Player");
      inventory = player.GetComponent<Inventory>();
      objects = null;
      name = "";
    }

    void LateUpdate()
     {
       checkHit();

       }

    void checkHit(){
      // Bit shift the index of the layer (8) to get a bit mask
      int layerMask = 0 << 8;
      RaycastHit hit;

      // This would cast rays only against colliders in layer 8.
      // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
      layerMask = ~layerMask;


      // Does the ray intersect any objectss excluding the player layer
      if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Range, layerMask))
      {
          objects = hit.transform.gameObject;
          checkObjects();

      }else{
        objects = null;
        use = null;
        name = "";
        collectable = false;
        useable = false;
        }

    }

    void checkObjects(){
      if(objects.GetComponent<PickUp>() != null){
        name = objects.name;
        pick = objects.GetComponent<PickUp>();
        collectable = inventory.checkInventory(name);
        useable = false;
      }else if(objects.GetComponent<EObject>()){
        name = objects.name;
        use = objects.GetComponent<EObject>();
        useable = true;
        collectable = false;

      }
      else{
        name = "";
        pick = null;
        use = null;
        collectable = false;
        useable = false;
      }
    }

    public string getObjectName(){
      return name;
      }

    public void UseObject(){
      if(objects != null){
        if(collectable == true)  pick.collect(inventory);
        if(useable == true) use.Use();
      }
    }
  }
