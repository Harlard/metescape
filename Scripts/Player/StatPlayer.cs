using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatPlayer : MonoBehaviour
{

    private int currentHealth;
    [SerializeField]public int maxHealth;
    private int currentStamina;
    [SerializeField]public int maxStamina;
    [SerializeField]public int staminaRecoverRate;
    [SerializeField]public int staminaUseRate;
    private float staminaSpendTime;
    private float staminaRecoverTime;
    [SerializeField]public bool isPoisoned;
    [SerializeField]public float poisonRate;
    [SerializeField]public int poisonDamage;
    private float poisonTime;
    [SerializeField]public bool isStun;
    [SerializeField]public float stunRecoverTime;
    private float stunTime;
    [SerializeField]public bool isFatigate;
    [SerializeField]public bool isDead;
    [SerializeField]public float recoverDelay;
    [SerializeField]public int recoverAmount;
    [SerializeField]public float recoverSpeed;
    private bool recentlyDamage;
    private float recoverCold;
    private ControllerM controller;

    // Start is called before the first frame update
    void Awake()
    {
        controller = GetComponent<ControllerM>();
        currentHealth = maxHealth;
        currentStamina = maxStamina;
        poisonTime = 0;
        recoverCold = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CheckState();
    }

    public void TakeDamage(int amount){
      currentHealth -= amount;
      if(currentHealth<=0) isDead = true;
      if(currentHealth>=maxHealth){
      currentHealth = maxHealth;}
      recentlyDamage = true;
      recoverCold = recoverDelay;
        }

    private void Recovering(){
      if((!recentlyDamage)&&(!isPoisoned)&&(currentHealth <= maxHealth)){
        recoverCold -= recoverSpeed*Time.deltaTime;
        if(recoverCold <= 0.0){
          currentHealth += recoverAmount;
          recoverCold = 100;
        }}
      if(recentlyDamage){
        recoverCold -= 1*Time.deltaTime;
        if(recoverCold<=0){
          recentlyDamage = false;
          recoverCold = 100;
        }
      }
    }

    public int GetCurrentHealth(){return currentHealth;}
    public int GetMaxHealth(){return maxHealth;}
    public int GetCurrentStamina(){return currentStamina;}
    public int GetMaxStamina(){return maxStamina;}
    public void UseStamina(){
      staminaSpendTime += staminaUseRate*Time.deltaTime;
      if(staminaSpendTime >= 100){
        currentStamina -= 1;
        staminaSpendTime = 0;
        if(currentStamina <= 0){
          isFatigate = true;
          controller.SetFatigate(false);
        }
      }
    }
    public void RecoverStamina(){
      if(currentStamina < maxStamina){
        staminaRecoverTime += staminaRecoverRate*Time.deltaTime;
        if(staminaRecoverTime >= 100){
          currentStamina += 1;
          staminaRecoverTime =0;
          if(currentStamina >= maxStamina){
            currentStamina = maxStamina;
          }
        }
      }
    }

    public void SetPoison(bool state){isPoisoned = state;}
    private void Poisoned(){
      poisonTime += poisonRate*Time.deltaTime;
      if(poisonTime > 100){
        TakeDamage(poisonDamage);
        poisonTime = 0;
      }
    }
    public void SetStun(){
      isStun = true;
      stunTime = stunRecoverTime;}


    private void Stun(){
      stunTime -= 1*Time.deltaTime;
      if(stunTime<=0){
        isStun=false;
        controller.SetStun(false);
      }
    }


    private void Fatigate(){
        if(currentStamina >= maxStamina){
          isFatigate = false;
          controller.SetFatigate(false);
        }
    }

    private void Dead(){}

    private void CheckState(){
      if(isDead == true){ Dead();
      }else{
      if(isPoisoned == true) Poisoned();
      if(isFatigate == true) Fatigate();
      if(isStun == true) Stun();
      Recovering();
    }
    }
    }
