using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightVM : MonoBehaviour
{
  public GameObject TorchLight;
  private bool TorchOn = false;
  public ParticleSystem lightBurst;
  [SerializeField]public int damage;
  [SerializeField]public float damageRate;
  private float damageTime;
  [SerializeField] public float damageRange;
  private GameObject objects;
  private Enemy enemy;
  private int currentBattery;
  [SerializeField]public int maxBattery;
  [SerializeField]public float spendRate;
  private float spendTime;
  bool playerMoving;


    // Start is called before the first frame update
    void Start()
    {
        TorchLight.gameObject.SetActive(false);
        currentBattery = maxBattery;
        damageTime = 0;
        spendTime = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if(TorchOn){
          checkHit();
          BatteryUse();
          }

    }


      public void ActiveFlashling(){
          if ((TorchOn ==false)&&(currentBattery>0))
            {
                TorchLight.gameObject.SetActive(true);
                TorchOn = true;
                lightBurst.Play();
            }
            else
            {
                TorchLight.gameObject.SetActive(false);
                TorchOn = false;
                lightBurst.Stop();
            }
        }


      void checkHit(){
        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 0 << 8;
        RaycastHit hit;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;


        // Does the ray intersect any objectss excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, damageRange, layerMask))
        {
          objects = hit.transform.gameObject;
          checkObjects();
          }else{
            objects = null;
            }}

        void checkObjects(){
          if(objects.GetComponent<Enemy>() != null){
            enemy = objects.GetComponent<Enemy>();
            damageTime += damageRate*Time.deltaTime;
            if(damageTime > 100){
              enemy.TakeDamage(damage);
              damageTime = 0;
              }
          }else{
            objects = null;
            }}

        void BatteryUse(){
          spendTime += spendRate*Time.deltaTime;
          if((spendTime > 100.0)&&(currentBattery>0)){
            currentBattery += -1;
            spendTime = 0;
          }
          if(currentBattery==0){
            TorchOn = false;
            TorchLight.gameObject.SetActive(false);
          }
        }

        public void rechargeBattery(int amount){
          currentBattery = amount;
        }

        public int GetCurrentBattery(){
          return currentBattery;
        }
        public int GetMaxBattery(){
          return maxBattery;
        }
}
