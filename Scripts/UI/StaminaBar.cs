using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public Slider slider;

    private int maxStamina = 101;
    private int currentStamina;

    private WaitForSeconds regenTick = new WaitForSeconds(0.1f);
    private WaitForSeconds depletionTick = new WaitForSeconds(0.1f);
    private Coroutine regen;

    public static StaminaBar instance;

    private void Awake()
    {
        instance = this;
        slider = gameObject.GetComponent<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentStamina = 100;
        slider.maxValue = maxStamina;
        slider.value = maxStamina;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            StartCoroutine(UseStamina());
            StopCoroutine(RegenStamina());
        }

        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            StopCoroutine(UseStamina());
            StartCoroutine(RegenStamina());
        }
    }

    //Decreases the stamina bar's value while the left mouse button is held down
    private IEnumerator UseStamina()
    {
        

        while (Input.GetKey(KeyCode.LeftShift))
        {
            currentStamina -= maxStamina / 101;
            slider.value = currentStamina;
            yield return depletionTick;
        }
    }

    //Regens the stamina bar's value while the left mouse button is no longer held down and the current value of stamina is less than the max stamina
    private IEnumerator RegenStamina()
    {
        yield return new WaitForSeconds(0.1f);

        while (! Input.GetKey(KeyCode.LeftShift) & currentStamina < maxStamina)
        {
            currentStamina += maxStamina / 101;
            slider.value = currentStamina;
            yield return regenTick;
        }
        regen = null;
    }
}
