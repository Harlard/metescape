using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowNameTest : MonoBehaviour
{
    private Text text;
    private GameObject camera;
    private PickingObject looking;
    private string name;
    // Start is called before the first frame update
    void Awake()
    {
        text = GetComponent<Text>();
        camera = GameObject.Find("Main Camera");
        looking = camera.GetComponent<PickingObject>();
        name = "";

    }

    // Update is called once per frame
    void LateUpdate()
    {
      name = looking.getObjectName();
      if(name != null){
        text.text = name;
      }else{
        name = "";
      }
    }
}
