using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FlashlightBarVM : MonoBehaviour
{
    public Slider slider;
    private GameObject player;
    private Flashlight flashlight;

    public static FlashlightBarVM instance;

    void Awake(){
      instance = this;
      slider = gameObject.GetComponent<Slider>();
      player = GameObject.Find("Player");
      flashlight = player.GetComponent<Flashlight>();

    }
    void Start()
    {
        slider.maxValue = flashlight.GetMaxBattery();
        slider.value = flashlight.GetCurrentBattery();
    }

    void LateUpdate(){
      slider.value = flashlight.GetCurrentBattery();
      Debug.Log(slider.value);
    }

}
