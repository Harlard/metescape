using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Slider slider;

    private int maxHealth = 101;
    private int currentHealth = 25;

    public static Healthbar instance;

    private void Awake()
    {
        instance = this;
        slider = gameObject.GetComponent<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = 25;
        slider.maxValue = maxHealth;
        slider.value = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth < maxHealth & Input.GetKeyDown(KeyCode.Mouse1)) // & the player has walked over/used a medkit)
        {
            currentHealth += 50;
        }
    }
}
