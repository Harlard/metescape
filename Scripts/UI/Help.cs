using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Help : MonoBehaviour
{
    private Text text;
    public float timeOnScreen;
    private float timeCold;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
       if(text.text != ""){
          timeCold -= 1*Time.deltaTime;
          if(timeCold<=0){
            text.text = "";
          }
        }
    }

    public void SetText(string hint){
      text.text =  hint;
      timeCold = timeOnScreen;
    }
}
