using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthBarVM : MonoBehaviour
{
    public Slider slider;
    private GameObject player;
    private StatPlayer stat;

    public static HealthBarVM instance;

    void Awake(){
      instance = this;
      slider = gameObject.GetComponent<Slider>();
      player = GameObject.Find("Player");
      stat = player.GetComponent<StatPlayer>();

    }
    void Start()
    {
        slider.maxValue = stat.GetMaxHealth();
        slider.value = stat.GetCurrentHealth();
    }

    void LateUpdate(){
      slider.value = stat.GetCurrentHealth();
    }

}
