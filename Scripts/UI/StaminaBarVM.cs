using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StaminaBarVM : MonoBehaviour
{
    public Slider slider;
    private GameObject player;
    private StatPlayer stat;

    public static StaminaBarVM instance;

    void Awake(){
      instance = this;
      slider = gameObject.GetComponent<Slider>();
      player = GameObject.Find("Player");
      stat = player.GetComponent<StatPlayer>();

    }
    void Start()
    {
        slider.maxValue = stat.GetMaxStamina();
        slider.value = stat.GetCurrentStamina();
    }

    void LateUpdate(){
      slider.value = stat.GetCurrentStamina();
    }

}
