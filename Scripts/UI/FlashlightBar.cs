using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashlightBar : MonoBehaviour
{
    public Slider slider;

    private int maxBatteryLevel = 101;
    private int currentBatteryLevel;

    private WaitForSeconds regenTick = new WaitForSeconds(0.1f);
    private WaitForSeconds depletionTick = new WaitForSeconds(0.1f);
    private Coroutine regen2;

    public static FlashlightBar instance;

    private void Awake()
    {
        instance = this;
        slider = gameObject.GetComponent<Slider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        currentBatteryLevel = 100;
        slider.maxValue = maxBatteryLevel;
        slider.value = maxBatteryLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) == true)
        {
            StartCoroutine(UseFlashlight());
            StopCoroutine(RegenFlashlight());
        }

        else if (Input.GetKeyUp(KeyCode.Mouse0) == true)
        {
            StopCoroutine(UseFlashlight());
            StartCoroutine(RegenFlashlight());
        }
    }

    //Decreases the flashlight bar's value while the left mouse button is held down
    private IEnumerator UseFlashlight()
    {
        while (Input.GetKey(KeyCode.Mouse0))
        {
            currentBatteryLevel -= maxBatteryLevel / 101;
            slider.value = currentBatteryLevel;
            yield return depletionTick;
        }
    }

    //Regens the flashlight bar's value while the left mouse button is no longer held down and the current value of the battery level is less than the max battery level
    private IEnumerator RegenFlashlight()
    {
        yield return new WaitForSeconds(0.1f);

        while (! Input.GetKey(KeyCode.Mouse0) & currentBatteryLevel < maxBatteryLevel)
        {
            currentBatteryLevel += maxBatteryLevel / 101;
            slider.value = currentBatteryLevel;
            yield return regenTick;
        }
        regen2 = null;
    }
}
