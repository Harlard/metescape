using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Farter2 : Enemy
{
  private bool isFarting;
  private bool isBurping;
  private bool isRunning;
  private bool drop = true;
  [SerializeField]public float attackRange;
  [SerializeField]private int burpDamage;
  [SerializeField]public GameObject trophy;
  [SerializeField]public GameObject coins;
  [SerializeField]public float rangeChecks;


  public override void Death(){
    if(drop){
    MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
    renderer.enabled = false;
    Instantiate(trophy, transform.position, Quaternion.identity);
    Instantiate(coins, transform.position, Quaternion.identity);
    drop = false;
  }else{}
  }
  public override void FightPlayer(){
      if(!activeThreshold){
        if(canSeePlayer){
        transform.LookAt(playerT);
        currentTBR = timeBeforeRunning;
        float distance = Vector3.Distance(transform.position, playerT.transform.position);
       if(distance <= attackRange){
         isRunning = false;
         if(currentTBA<=0){
         isBurping = true;
         statPlayer.TakeDamage(burpDamage);
         statPlayer.SetStun();
         currentTBA = timeBetweenAttack;
         }else{
           currentTBA = 1*Time.deltaTime;
         }
       }else{
         transform.position = Vector3.forward * Time.deltaTime * speed;
         isRunning = true;
         isBurping = false;
        }
    }else{
      currentTBR -= 1*Time.deltaTime;
      if(currentTBR<=0){
        fightingPlayer = false;
        isRunningAway = true;
      }
    }
  }else{
    activeThreshold = false;
    isFarting = true;
    statPlayer.SetPoison(true);
  }
  }
  public override void RunningAway(){
    lookingForRoute();
  }
  public override void Patrolling(){
    lookingForRoute();
  }

  public override void Stun(){
    transform.position = Vector3.back * Time.deltaTime * (speed/3);
  }

  public void lookingForRoute(){
    int layerMask = 0 << 8;
    RaycastHit hit;

    // This would cast rays only against colliders in layer 8.
    // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
    layerMask = ~layerMask;


    // Does the ray intersect any objectss excluding the player layer
    if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, rangeChecks, layerMask))
    {
        if(hit.transform.name == "F Block Layout"){
          transform.Rotate(0.0f, speed*Time.deltaTime, 0.0f);
          isRunning = false;
        }else{
          transform.position = Vector3.forward * Time.deltaTime * speed;
          isRunning = true;

      }

  }
}
}
