using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Farter : MonoBehaviour
{
    private bool isFarting;
    private bool isBurping;
    private bool isRunning;

    public NavMeshAgent agent;
    public Transform player;
    public LayerMask whatIsGround, whatIsPlayer;
    public Vector3 walkPoint;
    bool walkpoint;
    public float walkPointRange;
    public float timeBetweenAttack;
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange, walkPointSet, alreadyAttack;


    // Start is called before the first frame update
    private void Awake()
    {
      player = GameObject.Find("Player").transform;
      agent = GetComponent<NavMeshAgent>();
    }

    private void Update(){
      playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
      playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

      if(!playerInSightRange && !playerInAttackRange) Patroling();
      if(playerInSightRange && !playerInAttackRange) ChasePlayer();
      if(!playerInSightRange && !playerInAttackRange) AttackPlayer();
    }

    // Update is called once per frame
    private void Patroling()
    {
      if(!walkPointSet) SearchWalkPoint();
      if(walkPointSet) agent.SetDestination(walkPoint);
      Vector3 distanceToWalkPoint = transform.position - walkPoint;

      if (distanceToWalkPoint.magnitude < 1f)
        walkPointSet = false;
    }
    private void SearchWalkPoint(){
      float randomZ = Random.Range(-walkPointRange, walkPointRange);
      float randomX = Random.Range(-walkPointRange, walkPointRange);

      walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

      if(Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround)) walkPointSet = true;
    }
    private void ChasePlayer()
    {
      agent.SetDestination(player.position);
    }
    private void AttackPlayer()
    {
      transform.LookAt(player);
      if(!alreadyAttack){

        // burping or Farting

        alreadyAttack = true;
        Invoke(nameof(ResetAttack), timeBetweenAttack);
      }
    }

    private void ResetAttack(){
      alreadyAttack = false;
    }


}
