using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy {
  void FightPlayer();
  void Patrolling();
  void Stun();
  void RunningAway();

}


public abstract class Enemy : MonoBehaviour, IEnemy
{
  [SerializeField] public int maxHealth;
  [SerializeField] public int currentHealth;
  [SerializeField] public float currentStunRecover;
  [SerializeField] public float stunRecoverTime;
  [SerializeField] public float speed;
  [SerializeField]public float timeBeforeRunning;
  [SerializeField]public float timeBetweenAttack;
  public float currentTBA;
  public float currentTBR;

  public StatPlayer statPlayer;
  private bool isStun;
  public bool fightingPlayer;
  public bool isRunningAway;
  private bool isAlive;
  public bool threshold = false;
  public bool activeThreshold;


  public Transform playerT;
  public float radius;
  [Range(0,360)]
  public float angle;
  public GameObject player;
  public LayerMask targetMask;
  public LayerMask obstructionMask;

  public Animation anim;


  public bool canSeePlayer;

    // Start is called before the first frame update
    private void Awake()
    {
      currentTBA = 0;
      currentTBR = timeBeforeRunning;
      player = GameObject.Find("Player");
      statPlayer = player.GetComponent<StatPlayer>();
      playerT = player.GetComponent<Transform>();
      isStun = false;
      fightingPlayer = false;
      isRunningAway = false;
      isAlive = true;
      anim = GetComponent<Animation>();
      foreach (AnimationState state in anim)
        {
            state.speed = 0.5F;
        }
      StartCoroutine(FOVRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        if(!isAlive){
          Death();
        }
        else if(isStun){
          Stun();
          RecoveringFromStun();
        }else if(isRunningAway){
          RunningAway();
        }
        else if(fightingPlayer){
          FightPlayer();
        }
        else{
          Patrolling();
        }
    }

    public abstract void Death();
    public abstract void FightPlayer();
    public abstract void RunningAway();
    public abstract void Patrolling();
    public abstract void Stun();

    public void TakeDamage(int amount){
      currentHealth -= amount;
      if(!isRunningAway) fightingPlayer = true;
      if((!threshold)&&(currentHealth<=50)){
        threshold = true;
        activeThreshold = true;
      }
      if(currentHealth<=0) isAlive = false;
    }

    public void SetStun(){
      if(!isStun) isStun = true;
      currentStunRecover = stunRecoverTime;
    }

    public void RecoveringFromStun(){
      if(currentStunRecover > 0)  currentStunRecover += -(1 * Time.deltaTime);
      else{isStun = false;}
    }

    private IEnumerator FOVRoutine()    {
        WaitForSeconds wait = new WaitForSeconds(0.2f);

        while (true)
        {
            yield return wait;
            FieldOfViewCheck();
        }
    }

    private void FieldOfViewCheck()    {
        Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask);

        if (rangeChecks.Length != 0)
        {
            Transform target = rangeChecks[0].transform;
            Vector3 directionToTarget = (target.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstructionMask))
                    canSeePlayer = true;
                else
                    canSeePlayer = false;
            }
            else
                canSeePlayer = false;
        }
        else if (canSeePlayer)
            canSeePlayer = false;
    }
}
