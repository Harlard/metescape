using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antidote : PickUp
{
  [SerializeField] private int antidoteAmount;

  public override void collect(Inventory player){
    player.addAntidote(antidoteAmount);
  }
}
