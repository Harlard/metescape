using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : PickUp
{
    [SerializeField] private int keyNumber;

public override void collect(Inventory player){
  player.addKey(keyNumber);
}

  }
