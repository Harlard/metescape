using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickUp {
  void collect(Inventory player);
}

public abstract class PickUp : MonoBehaviour, IPickUp{

  public abstract void collect(Inventory player);

  private bool collected;

  public void Awake(){
      collected =false;
  }

  public void picked(Inventory player){
    if(collected == true) return;
    else{
    this.collect(player);
    collected = true;
    MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
    renderer.enabled = false;}
  }
}
