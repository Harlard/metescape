using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Batteries : PickUp
{
  [SerializeField] private int batteriesAmount;

  public override void collect(Inventory player){
    player.addBattery(batteriesAmount);
  }
}
