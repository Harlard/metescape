using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : PickUp
{
  [SerializeField] private int coinsAmount;

  public override void collect(Inventory player){
    player.addCoins(coinsAmount);
  }
}
