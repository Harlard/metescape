using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealhtPack : PickUp
{
  [SerializeField] private int healthPackAmount;

  public override void collect(Inventory player){
    player.addHealthPack(healthPackAmount);
  }
}
