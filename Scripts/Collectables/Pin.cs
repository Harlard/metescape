using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : PickUp
{

  [SerializeField] private int pinN;

  public override void collect(Inventory player){
    player.addPin(pinN);
  }
}
