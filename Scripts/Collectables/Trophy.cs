using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Trophy : PickUp
{

  [SerializeField] private int trophyN;

  public override void collect(Inventory player){
    player.addTrophy(trophyN);
  }
}
